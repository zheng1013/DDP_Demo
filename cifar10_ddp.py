import torch
from torch.utils.tensorboard.summary import image
import torchvision
import torch.nn.functional as F
import torch.nn as nn
import torchvision.transforms as transforms
import torch.optim as optim
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import argparse,random
import numpy as np
from tqdm import tqdm


#訓練參數
#---------------------------------------------------------------

Batch_Size = 32 
Nodes = 4
Learning_Rate=0.001 * Nodes * (Batch_Size/32)
Epoch = 10
iterator = tqdm(range(Epoch))



#種子函數
#---------------------------------------------------------------
def init_seeds(seed=0, cuda_deterministic=True):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)

    if cuda_deterministic:  
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    else:  
        torch.backends.cudnn.deterministic = False
        torch.backends.cudnn.benchmark = True



#訓練流程
#---------------------------------------------------------------
def train_epoch(train_loader, optimizer, criterion, model, world_size):
    
    #訓練流程
    model.train()

    
    training_loss = 0.0
    training_acc = 0.0
    for _step, input_data in enumerate(train_loader):

        image, label = input_data[0].to(local_rank, non_blocking=True), input_data[1].to(local_rank, non_blocking=True)   
        optimizer.zero_grad()
        predict_label = model(image)
        preds = torch.max(predict_label, 1)[1]
        loss = criterion(predict_label, label)
        loss.backward()
        iterator.desc = "loss = %0.3f" % loss
        optimizer.step()
        training_loss = training_loss + loss.item()
        training_acc += torch.eq(preds, label).sum().item()
        if _step % 10 == 0 :
            print('[iteration - %3d] training loss: %.3f' % (_epoch*len(train_loader) + _step, training_loss/10))
            training_loss = 0.0
            print()


    train_loss_value = training_loss/ (len(train_dataset) / world_size)
    train_acc_value = training_acc/ (len(train_dataset) / world_size)
    return train_loss_value, train_acc_value



def valid_epoch(valid_loader, criterion, model, world_size):

    model.eval()

    valid_running_loss = 0.0
    valid_running_acc = 0.0
    with torch.no_grad():
        for batch_idx, (data, target) in enumerate(valid_loader):
            data = data.to(device, non_blocking=True)
            target = target.to(device, non_blocking=True)

            outputs = model(data)
            preds = torch.max(outputs, 1)[1]

            loss = criterion(outputs, target)

            valid_running_loss += loss.item() 
            valid_running_acc += torch.eq(preds, target).sum().item()

    valid_loss_value = valid_running_loss/ (len(valid_dataset) / world_size)
    valid_acc_value = valid_running_acc/ (len(valid_dataset) / world_size)
    return valid_loss_value, valid_acc_value

if __name__ == '__main__':
    #---------------------------------------------------------------
    ### 初始化模型、資料、配置  
    # DDP：從外部得到local_rank參數
    parser = argparse.ArgumentParser()
    parser.add_argument("--local_rank", default=0, type=int)
    FLAGS = parser.parse_args()
    local_rank = FLAGS.local_rank
    # DDP：DDP backend初始化
    torch.cuda.set_device(local_rank)
    # 種子函數
    init_seeds(1 + local_rank)
    dist.init_process_group(backend='nccl')  
    dist.barrier()

    world_size = dist.get_world_size()
    #---------------------------------------------------------------
    #Data Tranforms
    myTransforms = transforms.Compose([
    transforms.Resize((224, 224)),
    transforms.RandomHorizontalFlip(p=0.5),
    transforms.ToTensor(),
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))])


    #資料集
    #--------------------------
    #Load DataSet

    #Train
    train_dataset = torchvision.datasets.CIFAR10(root='./cifar-10-python/', train=True, download=True, transform=myTransforms)
    train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)\
    #新增pin_memory=False, prefetch_factor=2, num_workers=4
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=Batch_Size,sampler=train_sampler,drop_last=True,pin_memory=False, prefetch_factor=2,num_workers=4)




    ##Test
    test_dataset = torchvision.datasets.CIFAR10(root='./cifar-10-python/', train=False, download=True, transform=myTransforms)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=4, shuffle=True, pin_memory=False, prefetch_factor=2, num_workers=4)

    if torch.cuda.is_available():
        local_rank = torch.device("cuda", FLAGS.local_rank)
    else:
        local_rank = torch.device("cpu")




    #模型
    #--------------------------
    #導入pytorch 原本就有包的Resnet50
    myModel = torchvision.models.resnet50(pretrained=True)
    inchannel = myModel.fc.in_features
    myModel.fc = nn.Linear(inchannel, 10)





    # #指定運算裝置CPU/GPU/節點
    myModel = myModel.to(local_rank)

    myModel = torch.nn.SyncBatchNorm.convert_sync_batchnorm(myModel)
    myModel = DDP(myModel, device_ids=[local_rank], output_device=local_rank)


    #lr 跟優化器 Loss函數
    myOptimzier = optim.SGD(myModel.parameters(), lr = Learning_Rate, momentum=0.9)
    myLoss = torch.nn.CrossEntropyLoss().to(local_rank)


    for _epoch in range(Epoch):
        train_loader.sampler.set_epoch(_epoch)

        train_loss_value, train_acc_value = train_epoch(train_loader, myOptimzier, myLoss , myModel, world_size)
        print("Train_local_rank: {} Train_Epoch: {}/{} Training_Loss: {} Training_acc: {:.2f}\
                ".format(local_rank, _epoch, Epoch-1, train_loss_value, train_acc_value))


    '''
    # 保存整個模型
    if dist.get_rank() == 0:
        torch.save(myModel.module.state_dict(), "%d.ckpt" % _epoch)



    print("finished.")
    
    valid_loss_value, valid_acc_value = valid_epoch(test_loader, myLoss, myModel, world_size)    
    print("Valid_local_rank: {} Valid_Epoch: {}/{} Valid_Loss: {} Valid_acc: {:.2f}\
            ".format(local_rank,_epoch, Epoch-1, valid_loss_value, valid_acc_value))
    '''
